#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  idle_time.py
#  
from ctypes import Structure, windll, c_uint, sizeof, byref

class LASTINPUTINFO(Structure):
	_fields_ = [
		('cbSize', c_uint),
		('dwTime', c_uint),
	]

def duration():
	lastInputInfo = LASTINPUTINFO()
	lastInputInfo.cbSize = sizeof(lastInputInfo)
	windll.user32.GetLastInputInfo(byref(lastInputInfo))
	millis = windll.kernel32.GetTickCount() - lastInputInfo.dwTime
	return int(millis / 1000.0)
