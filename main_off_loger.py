#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from tkinter import ttk, _tkinter
import tkinter as tk
import images as imgs
import down_counter 
import webbrowser
import user
import os

project_url = 'https://gitlab.com/fredmain/offloger'

icon_img_data = imgs.icon_img

commands_dict = {
"Lock screen":'Rundll32.exe User32.dll,LockWorkStation', 
"Hibernate":'rundll32.exe PowrProf.dll,SetSuspendState', 
"Shutdown":'shutdown /s'
}


class App():
	def __init__(self, root, *args, **kwargs):
		self.root = root
		self.root.geometry("340x220")
		self.root.minsize(340,220)
		self.root.title('Offloger')
		self.root.iconphoto(False, tk.PhotoImage(data=icon_img_data))

		self.down_count = down_counter.Count(self, self.action)


		main_frame = tk.Frame(self.root, pady=5, padx=5)
		main_frame.pack(fill='both', expand=True)


		options_frame = tk.Frame(main_frame)
		options_frame.pack(anchor='w', fill='both', expand=True)


		## Options row 1 frame -----------------------------------------
		options_row_1 = tk.Frame(options_frame)
		options_row_1.pack(anchor='w', )

		label_1 = tk.Label(options_row_1, text='After ')

		self.minutes = tk.Entry(options_row_1, width=3, highlightthickness=1, 
		highlightbackground='gray75', bd=0)
		self.minutes.focus()
		self.minutes.bind("<KeyRelease>", self.down_count.start_stop)
		
		
		self.label_2 = tk.Label(options_row_1, text='minutes of inactivity ')

		self.combo = ttk.Combobox(options_row_1, width=17, state="readonly",
		values=list(commands_dict))
		self.combo.set(list(commands_dict)[0])

		for widget in options_row_1.winfo_children(): widget.pack(side='left')
		##--------------------------------------------------------------


		## Options row 2 frame -----------------------------------------
		options_row_2 = tk.Frame(options_frame)
		options_row_2.pack(anchor='w')

		self.do_one_time = tk.IntVar()
		do_one_time_cb = tk.Checkbutton(options_row_2, text="Just once.", 
		variable=self.do_one_time)
		do_one_time_cb.pack(side='left')
		##--------------------------------------------------------------


		#Start Button --------------------------------------------------
		self.start_bt = tk.Button(main_frame, text='Start', relief='groove', 
		command=self.down_count.start_stop, height=3, font=(None, 15))
		self.start_bt.pack(fill='both', expand=True)
		self.root.bind("<Return>", self.down_count.start_stop)
		self.start_bt.bind("<Enter>", lambda event, bt=self.start_bt: bt.configure(bg="azure"))
		self.start_bt.bind("<Leave>", lambda event, bt=self.start_bt: bt.configure(bg="white"))
		#---------------------------------------------------------------


		##Status and about area ----------------------------------------
		status_about_frame = tk.Frame(main_frame)
		status_about_frame.pack(fill='both')

		status_frame = tk.Frame(status_about_frame)
		status_frame.pack(fill='both', side='left')

		self.status_label = tk.Label(status_frame, text='Status: Stopped.')
		self.status_label.pack(side='left')

		self.separator = ttk.Separator(status_about_frame, orient='vertical')#---------------

		self.about_bt = tk.Button(status_about_frame, text='About', relief='flat',
		command=lambda url=project_url: webbrowser.open(url))
		self.about_bt.pack(side='right')
		self.about_bt.bind("<Enter>", lambda event, bt=self.about_bt: bt.configure(bg="azure"))
		self.about_bt.bind("<Leave>", lambda event, bt=self.about_bt: bt.configure(bg="white"))

		self.separator.pack(side='right', fill='y')
		##--------------------------------------------------------------


		self.all_widgets_config(self.root)

		for child in main_frame.winfo_children():
			child.configure(highlightthickness=1, highlightcolor='gray85', 
			highlightbackground='gray85', pady=5, padx=5)
			child.pack_configure(pady=2)



	def all_widgets_config(self, parent):
		for widget in parent.winfo_children():
			try:
				widget.config(background='white')
			except _tkinter.TclError:
				pass

			if widget.winfo_class() == 'Frame':
				self.all_widgets_config(widget)

	
	def action(self):
		os.system('cls')
		os.system(commands_dict[self.combo.get()])

		def check_user_status():
			if user.is_locked():
				self.root.after(1000, check_user_status)
			else:
				self.down_count.start_stop()


		if self.do_one_time.get():
			pass
		else:
			check_user_status()




if __name__ == "__main__":
	root = tk.Tk()
	App(root)
	root.mainloop()
