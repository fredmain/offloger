#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import idle_time


class Count:
	def __init__(self, app, action):
		self.state = 0
		self.app = app
		self.action = action
		self.stopped_status_text = 'Status: Stopped.'
		self.default_label_2_text = 'minutes of inactivity '


	def valid_input(self):
		if self.app.minutes.get().isdigit():
			return int(self.app.minutes.get()) * 60
		else:
			self.app.minutes.delete(0, 'end')
			return None
		

	def start_stop(self, *args):
		if args:
			event = args[0]
			widget_type = event.widget.winfo_class()
		else:
			widget_type = None

		self.given_seconds = self.valid_input()
		
		if self.given_seconds:
			if self.given_seconds == 60:
				self.app.label_2.configure(text=self.app.label_2['text'].replace('minutes','minute'))
			else:
				self.app.label_2.configure(text=self.default_label_2_text)


			self.remaining_seconds = self.given_seconds

			if self.state == 1 and widget_type != 'Entry': 
				self.state = 0
				self.app.start_bt.config(text='Start')
				self.app.status_label.config(text= self.stopped_status_text)

			elif self.state == 0:
				self.state = 1
				self.app.start_bt.config(text='Stop')
				self.retro_counter()



	def retro_counter(self):
		if self.state:
			if self.remaining_seconds == 0:
				self.state = 0
				self.app.start_bt.config(text='Start')
				self.app.status_label.config(text= self.stopped_status_text)	
				self.action()

			else:
				if self.valid_input():
					minutes, secs = divmod(self.remaining_seconds, 60) 
					remaining = '{:02d}:{:02d}'.format(minutes, secs)

					self.status = f"Status: Minutos restantes para {self.app.combo.get().lower()}: {remaining}"
					self.app.status_label.config(text=self.status)
					print(self.status, end="\r") 
					
					self.remaining_seconds = self.valid_input() - idle_time.duration()
					self.app.root.after(1000, self.retro_counter)
				else:
					self.state = 0
					self.app.start_bt.config(text='Start')
					self.app.status_label.config(text= self.stopped_status_text)		
		
	
	
	
	
	
	
	
	
	
	
